
.include "linux.s"

# CONVERSION-PROGRAM ##########################################################################
#                                                                                             #
# PURPOSE:   program that call some funcions to convert a decimal number given as argument in #
#             a string representation of its decimal, binary, octal and hexadecimal notation  #
#             and then displays it.                                                           #
#                                                                                             #
###############################################################################################



# DATA SECTION ________________________________________________________________________________
.section .data

# this is where it will be stored
notation_buffer:
  .ascii "\n\nnotations:\n\n"
integer_buffer:
  .ascii "- decimal -->       \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
binary_buffer:
  .ascii "- binary -->        \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
octal_buffer:
  .ascii "- octal -->         \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
hex_buffer:
  .ascii "- hexadecimal -->   \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"


# STACK POSITIONS FOR THE ARGUMENTS
.equ ST_ARGC, 0               # number of arguments
.equ ST_ARGV_0, 4             # name of program
.equ ST_ARGV_1, 8             # file name to write to
.equ ST_NUMBER, -4            # integer converted number
.equ ST_SIZE_RESERVE, 4       # size reserved for integer converted number on the stack

# OTHER CONSTANTS
.equ HEADER_OF_STRING, 20



# TEXT SECTION ________________________________________________________________________________
.section .text

# main program
.globl _start
_start:

movl %esp, %ebp

# allocate space for the integer number on the stack
subl $ST_SIZE_RESERVE, %esp

pushl ST_ARGV_1(%ebp)
call string2integer                               # call the function to convert the program argument to an integer
addl $4, %esp
movl %eax, ST_NUMBER(%ebp)                        # save the integer converted number to stack

pushl $HEADER_OF_STRING + integer_buffer          # storage for the result
pushl ST_NUMBER(%ebp)                             # number to convert
call integer2string
addl $8, %esp

pushl $HEADER_OF_STRING + binary_buffer           # storage for the result
pushl ST_NUMBER(%ebp)                             # number to convert
call integer2binary
addl $8, %esp

pushl $HEADER_OF_STRING + octal_buffer            # storage for the result
pushl ST_NUMBER(%ebp)                             # number to convert
call integer2octal
addl $8, %esp

pushl $HEADER_OF_STRING + hex_buffer              # storage for the result
pushl ST_NUMBER(%ebp)                             # number to convert
call integer2hex
addl $8, %esp



# here we start to print on stdout the output of the program: all string-converted results
# write the header line ("notation:\n") on stdout
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $notation_buffer, %ecx
movl $14, %edx
int $LINUX_SYSCALL

# get the character count for integer_buffer
pushl $integer_buffer
call count_chars
addl $4, %esp

# print on stdout the integer conversion of the number
movl %eax, %edx                   # the count goes in %edx for SYS_WRITE
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $integer_buffer, %ecx
int $LINUX_SYSCALL

# write a carriage return
pushl $STDOUT
call write_newline
addl $4, %esp

# get the character count for binary_buffer
pushl $binary_buffer
call count_chars
addl $4, %esp

# print on stdout the binary conversion of the number
movl %eax, %edx                   # the count goes in %edx for SYS_WRITE
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $binary_buffer, %ecx
int $LINUX_SYSCALL

# write a carriage return
pushl $STDOUT
call write_newline
addl $4, %esp

# get the character count for octal_buffer
pushl $octal_buffer
call count_chars
addl $4, %esp

# print on stdout the octal conversion of the number
movl %eax, %edx                   # the count goes in %edx for SYS_WRITE
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $octal_buffer, %ecx
int $LINUX_SYSCALL

# write a carriage return
pushl $STDOUT
call write_newline
addl $4, %esp

# get the character count for hex_buffer
pushl $hex_buffer
call count_chars
addl $4, %esp

# print on stdout the hexadecimal conversion of the number
movl %eax, %edx                   # the count goes in %edx for SYS_WRITE
movl $SYS_WRITE, %eax
movl $STDOUT, %ebx
movl $hex_buffer, %ecx
int $LINUX_SYSCALL

# write 3 times a carriage return
pushl $STDOUT
call write_newline
call write_newline
call write_newline
addl $4, %esp


# exit
movl $SYS_EXIT, %eax
movl $0, %ebx
int $LINUX_SYSCALL

