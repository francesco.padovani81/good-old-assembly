
# FUNCTION: COUNT_CHARS =========================================================
#                                                                               #
# PURPOSE:    Count the characters until a null byte is reached.                #
#                                                                               #
# INPUT:      The address of the character string                               #
#                                                                               #
# OUTPUT:     Returns the count in %eax                                         #
#                                                                               #
# PROCESS:    Registers used:                                                   #
#               %ecx --> character count                                        #
#               %al  --> current character                                      #
#               %edx --> current character address                              #
#                                                                               #
#================================================================================

.type count_chars, @function
.globl count_chars

# This is where our one parameter is on the stack
.equ ST_STRING_START_ADDRESS, 8

count_chars:
  pushl %ebp
  movl %esp, %ebp
  
  # Counter starts at zero
  movl $0, %ecx
  
  # Starting address of data
  movl ST_STRING_START_ADDRESS(%ebp), %edx

  count_loop_begin: #------------------------------------------
    movb (%edx), %al                        # grab the current character
    cmpb $0, %al                            # is it null?
    je count_loop_end                       # if yes we're done
    incl %ecx                               # otherwise increment the counter and the pointer
    incl %edx
    jmp count_loop_begin                    # go back to the beginning of the loop
  count_loop_end: #--------------------------------------------

  movl %ecx, %eax                           # we're done. Move the count into %eax and return
  popl %ebp
  ret

# END FUNCTION: COUNT_CHARS =====================================================

