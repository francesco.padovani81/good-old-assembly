
# FUNCTION: INTEGER2BINARY =================================================================
#                                                                                          #
# PURPOSE: Convert an integer number to its binary-string notation for display             #
#                                                                                          #
# INPUT:   - A buffer large enough to hold the largest possible number                     #
#          - An integer to convert                                                         #
#                                                                                          #
# OUTPUT:  The buffer will be overwritten with the binary string                           #
#                                                                                          #
# VARIABLES:                                                                               #
#          - %ecx will hold the count of characters processed                              #
#          - %eax will hold, at the beginning the current value, then, from time to time,  #
#             the result divided by 2 (shift-right of a bit)                               #
#                                                                                          #
#===========================================================================================

.equ ST_VALUE, 8
.equ ST_BUFFER, 12

.globl integer2binary
.type integer2binary, @function
integer2binary:

  # normal function beginning
  pushl %ebp
  movl %esp, %ebp

  xorl %ecx, %ecx                   # initialize current character count
  movl ST_VALUE(%ebp), %eax         # move the value into position


  conversion_loop: #---------------------------------------------------------
    shrl $1, %eax
    jc carry_one
    pushl $'0'
    jmp carry_evaluated
    carry_one:
    pushl $'1'
    carry_evaluated:
    incl %ecx                       # increment the digit count
    cmpl $0, %eax
    jg conversion_loop
  #end_conversion_loop: #-----------------------------------------------------

  # the string is now on the stack, if we pop it off a character at a time we can copy it
  #  into the buffer and be done.
  # Get the pointer to the buffer in %edx
  movl ST_BUFFER(%ebp), %edx

  copy_reversing_loop: #-----------------------------------------------------
    # we pushed a whole register, but we only need the last byte. So we're going to pop off
    #  to the entire %eax register, but then only move the small part (%al) into the 
    #  character string.
    popl %eax
    movb %al, (%edx)

    decl %ecx                     # decreasing %ecx so we know when we are finished

    incl %edx            # increasing %edx so that it will be pointing to the next byte

    cmpl $0, %ecx                # check to see if we are finished
    jg copy_reversing_loop   # if so, jump to the end of the function
  #end_copy_reversing_loop: #-------------------------------------------------

  # done copying. Now write a null byte and return
  movb $0, (%edx)

  movl %ebp, %esp
  popl %ebp
  ret

