
# FUNCTION: STRING2INTEGER =================================================================
#                                                                                          #
# PURPOSE: Convert a string to an integer number                                           #
#                                                                                          #
# INPUT:   The address of the string to convert                                            #
#                                                                                          #
# OUTPUT:  The integer obtained will be returned into the register %eax                    #
#                                                                                          #
# VARIABLES:                                                                               #
#          - %edx will hold the address of the string's byte to be processed               #
#          - %ecx will hold the result of conversion                                       #
#                                                                                          #
#===========================================================================================

.equ ST_STR_ADDR, 8

.globl string2integer
.type string2integer, @function
string2integer:

  # normal function beginning
  pushl %ebp
  movl %esp, %ebp

  movl ST_STR_ADDR(%ebp), %edx    # %edx holds the address of the first byte to be processed
  movl $0, %ecx                   # we clean %ecx to store the result. I contains previous converted number

  conversion_loop: #---------------------------------------------------------
    # the number conversion will be only in the low 8 bits of %eax register,
    #  so first clear out %eax
    movl $0, %eax

    movb (%edx), %al              # take the first byte and put it in the lowest byte of %eax
    subl $'0', %eax               # do the conversion by subtracting the '0' char index of the ASCII table
    imull $10, %ecx                # multiply by 10 the previous converted number (0 if no previous number)
    addl %eax, %ecx               # then add the new converted number to the previous (multiplied by 10)
    incl %edx                     # go to the next byte to process
    cmpb $0, (%edx)               # check to see if we are finished (the current byte is '\0')
    je end_conversion_loop        # if so, jump to the end of the conversion loop
    jmp conversion_loop           # else resume from the beginning
  end_conversion_loop:

  movl %ecx, %eax                 # save the result in %eax to return

  movl %ebp, %esp                 # return from function to the caller
  popl %ebp
  ret

