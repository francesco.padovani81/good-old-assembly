
# FUNCTION: INTEGER2HEX ====================================================================
#                                                                                          #
# PURPOSE: Convert an integer number to a hexadecimal string for display                   #
#                                                                                          #
# INPUT:   - A buffer large enough to hold the largest possible hexadecimal number         #
#          - An integer to convert                                                         #
#                                                                                          #
# OUTPUT:  The buffer will be overwritten with the hexadecimal string                      #
#                                                                                          #
# VARIABLES:                                                                               #
#          - %ecx will hold the count of characters processed                              #
#          - %eax will hold the current value                                              #
#                                                                                          #
#===========================================================================================

.equ ST_VALUE, 8
.equ ST_BUFFER, 12

.globl integer2hex
.type integer2hex, @function

integer2hex:

  # normal function beginning
  pushl %ebp
  movl %esp, %ebp

  xorl %ecx, %ecx                  # current character count
  movl ST_VALUE(%ebp), %eax        # move the value into position

  conversion_loop: #---------------------------------------------------------
    # divide the number by 16, by shifting 4 bit right (storing the result before)
    movl %eax, %edx
    andl $15, %edx
    shrl $4, %eax

    # quotient is in %eax. %edx has the reminder, which now needs to be converted
    #  into a number. If %edx is a number that is 0 through 9, then the ascii code for '0'
    #  plus %edx is the ascii code for the relative character. Otherwise we have to add
    #  further 7 to get the correct ascii character.
    cmpl $9, %edx
    jle is_digit
    addl $7, %edx
    is_digit:
    addl $'0', %edx

    # Now we will take this value and push it on the stack. This way, when we are done, we
    #  can just pop off the characters one-by-one and they will be in the right order. Note
    #  that we're pushing the whole register, but we only need the byte in %dl (the last byte
    #  of the %edx register) for the character.
    pushl %edx

    incl %ecx                     # increment the digit count

    # check to see if %eax is zero yet, go to next step if so.
    cmpl $0, %eax
    jg conversion_loop
  end_conversion_loop: #-----------------------------------------------------

  # the string is now on the stack, if we pop it off a character at a time we can copy it
  #  into the buffer and be done.
  # Get the pointer to the buffer in %edx
  movl ST_BUFFER(%ebp), %edx

  copy_reversing_loop: #-----------------------------------------------------
    # we pushed a whole register, but we only need the last byte. So we're going to pop off
    #  to the entire %eax register, but then only move the small part (%al) into the 
    #  character string.
    popl %eax
    movb %al, (%edx)

    decl %ecx                     # decreasing %ecx so we know when we are finished

    incl %edx                     # increasing %edx so that it will be pointing to the next byte

    cmpl $0, %ecx                 # check to see if we are finished
    jg copy_reversing_loop        # if so, jump to the end of the function
  end_copy_reversing_loop: #-------------------------------------------------

  # done copying. Now write a null byte and return
  movb $0, (%edx)

  movl %ebp, %esp
  popl %ebp
  ret

