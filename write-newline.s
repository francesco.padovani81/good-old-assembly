
.include "linux.s"

# FUNCTION: WRITE_NEWLINE =======================================================
#                                                                               #
# PURPOSE:   This function is to write a newline char on STDOUT                 #
#                                                                               #
#================================================================================

.type write_newline, @function
.globl write_newline

# DATA SECTION __________________________________________________________________
.section .data

newline:
  .ascii "\n"

# END DATA SECTION ______________________________________________________________



# TEXT SECTION __________________________________________________________________
.section .text

.equ ST_FILEDES, 8

write_newline:
  pushl %ebp
  movl %esp, %ebp
  
  movl $SYS_WRITE, %eax
  movl ST_FILEDES(%ebp), %ebx
  movl $newline, %ecx
  movl $1, %edx
  int $LINUX_SYSCALL
  
  movl %ebp, %esp
  popl %ebp
  ret
# END TEXT SECTION ______________________________________________________________

# END FUNCTION: WRITE_NEWLINE ===================================================

