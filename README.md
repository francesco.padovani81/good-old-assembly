# Assembly - Number Base Conversion

This is a simple assembly program to convert to convert a decimal number into a string representation of its decimal, binary, octal and hexadecimal notation and then display it on standard output.<br>
It has been written by using AT&T syntax for Linux i386 architecture, with mere learning purpose. All concepts and technics here used have been apprended by reading the book "Programming from the Ground Up (v1.0)", by Jonathan Bartlett (edited by Dominick Bruno, Jr.).

## Installation

### Ubuntu

Clone this repo and get inside the main folder. Then generate the binary file by running this commands (GNU assembler compiler is required):

```bash
as --32 conversion-program.s -o conversion-program.o
as --32 count-chars.s -o count-chars.o
as --32 integer-to-binary.s -o integer-to-binary.o
as --32 integer-to-hex.s -o integer-to-hex.o
as --32 integer-to-octal.s -o integer-to-octal.o
as --32 integer-to-string.s -o integer-to-string.o
as --32 linux.s -o linux.o
as --32 string-to-integer.s -o string-to-integer.o
as --32 write-newline.s -o write-newline.o
ld -m elf_i386 conversion-program.o count-chars.o integer-to-binary.o integer-to-hex.o integer-to-octal.o integer-to-string.o linux.o string-to-integer.o write-newline.o -o conversion-program
```

## Usage

Simply run the binary file in Terminal by passing the decimal number to convert as only argument:

![](images/usage-eg.png)

## License
Copyleft 2021 - whoever

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

